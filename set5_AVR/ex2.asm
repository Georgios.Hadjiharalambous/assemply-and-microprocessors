 	
	
.include "m16def.inc"
.def tempo = r16
.def leds = r17
.def A = r22
.def anamena = r23
.org 0x0
rjmp main
.org 0x2
rjmp intr0
.org 0x04
rjmp intr1
.org 0x10
rjmp ISR_TIMER1_OVF

main:
	ldi tempo,low(RAMEND)
	out SPL,tempo
	ldi tempo,high(RAMEND)
	out SPH, tempo
	ser tempo
	out DDRB,tempo			;B exodos
	
	;ldi r24,low(2000)
	;ldi r25,high(2000)			;r24:r25 for delay=0.5sec=500msec
	;out PORTB,tempo				;output B
	;rcall wait_msec
	clr tempo
	out DDRA,tempo			;A eisodos
	ldi tempo, (1<<INT1)		;energopoisi INT1
	out GIMSK,tempo
	ldi tempo, (1<<ISC11) | (1<<ISC10)
	ldi r24,(1<<TOIE1)
	out TIMSK,r24	;diakopi na ginetai sti thetikh akmi
	out MCUCR,tempo		
	ldi r24 ,(1<<CS12) | (0<<CS11) | (1<<CS10) ; CK/1024
	;sei
	out TCCR1B,r24 
	
	sei
	ldi anamena,0x00
	rjmp sensor
sensor:
sei
	in A,PINA
	andi A,0b10000000			;diavazw to PINA wste otan paithei to PA7 na anapsi to LED
	cpi A,0b10000000
	brne sensor	
	
PINA_on:
	in A,PINA
	andi A,0b10000000			;diavazw to PINA wste otan afethei to PA7 na anapsi to LED
	cpi A,0b10000000
	breq PINA_on				;oso paramenei patimeno perimene na afethei
	rjmp on
	
intr1:
	pop r24
	pop r24
;	push r24
;	push r25
loop:
	cli
	ldi r24, (1<<INTF1)			;GIFR = 1 wste na ginei graftei 0 k na anaferei pws i diakopi exei exipiretithei	
	out GIFR,r24
	ldi r24,low(5)
	ldi r25,high(5)			;r24:r25 for delay=0.005sec=5msec
	rcall wait_msec
	in r24,GIFR
	andi r24,0b10000000
	cpi r24,0b10000000
	sei
	breq loop
;	pop r25
;	pop r24
	;sei
	;breq intr1
	rjmp on

on:
	cpi anamena,0x1				;metavliti wste na xerw an ta LED einai idi anamena diladi den exoun perasei ta 4sec
	breq already_on
	ldi anamena,1				;anavw to LED k xrisimopoiw kataxwriti wste na xerw an einai anamena
	ldi tempo,0b00000001			;PB0
	out PORTB,tempo
	ldi tempo,0x85				;theloume o timer na metrisei 4sec => 4*7812.5 = 31250
	out TCNT1H,tempo				;h yperxeilish ginetai otan metrisei 65536 kyklous (16psifia)
	ldi tempo,0xEE				;tha prepei h arxikh timi na einai 65536-31250 = 34286 = 85EE 
	out TCNT1L,tempo
	rjmp sensor					;perimene na patithe to PA7

already_on:						;patithike enw itan idi anameno to PB0 => epanalipsi 4sec kai anama olwn ton LEDB
	cli							;apenergopoiw tis diakopes wste na min ginei kapoia diakopi sto wait
	ldi tempo,0b11111111			;anama olwn twn LEDB gia 0.5sec
	ldi r24,low(500)
	ldi r25,high(500)			;r24:r25 for delay=0.5sec=500msec
	out PORTB,tempo				;output B
	rcall wait_msec				;kalutera polla mikra waits giati mesa sta 0.5sec den boro na inter
	sei							;xanaenergopoiw tis diakopes
	ldi tempo,0b00000001			;anama tou PB0 gia 4sec
	out PORTB,tempo
	ldi tempo,0x95				;theloume o timer na metrisei 3.5sec => 3.5*7812.5 = 27343.75
	out TCNT1H,tempo				;h yperxeilish ginetai otan metrisei 65536 kyklous (16psifia)
	ldi tempo,0x30				;tha prepei h arxikh timi na einai 65536-27343.75 = 38192.25 = 9530 (hex)
	out TCNT1L,tempo
	rjmp sensor					;sinexis leitourgia -> perimenw patima tou PA7 wste na xanaenergopoithei to PB0
	


ISR_TIMER1_OVF:
	clr tempo			;otan teleiwsoun ta 4sec svinoyn ola ta LEDs
	out PORTB,tempo
	ldi anamena,0			;enimeronoyme pws den einai anamena <- exoyn perasei 4 sec
	;off timer
	reti

intr0:
	
wait_usec:
	sbiw r24 ,1 ; 2 circles (0.250 usec)
	nop ; 1 circle (0.125 usec)
	nop ; 1 circle (0.125 usec)
	nop ; 1 circle (0.125 usec)
	nop ; 1 circle (0.125 usec)
	brne wait_usec ; 1 or 2 circles (0.125 or 0.250 usec)
	ret ; 4 circles (0.500 usec)


wait_msec:
	push r24 ; 2 circles (0.250 usec)
	push r25 ; 2 circles
	ldi r24 , low(998) ; load r25:r24 with 998 (1 circle - 0.125 usec)
	ldi r25 , high(998) ; 1 circle (0.125 usec)
	rcall wait_usec ; 3 circles (0.375 usec), total delay 998.375 usec
	pop r25 ; 2 circles (0.250 usec)
	pop r24 ; 2 circles
	sbiw r24 , 1 ; 2 circles
	brne wait_msec ; 1 or 2 circles (0.125 or 0.250 usec)
ret ; 4 circles (0.500 usec)
